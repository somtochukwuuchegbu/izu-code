from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings
from .forms import RegisterForm, UserUploadForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import smart_bytes, smart_str, force_str, force_text, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse as activate_reverse


def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            firstname = form.cleaned_data["first_name"]
            lastname = form.cleaned_data["last_name"]
            email = form.cleaned_data.get("email")
            user = form.save()
            form = RegisterForm()
            user.is_active = False
            messages.success(
                request, f"Account created successfully for {firstname} {lastname} Visit your email for account activation link")
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = default_token_generator.make_token(user)
            current_site = get_current_site(request=request).domain
            relative_link = activate_reverse(
                'activate', kwargs={'uidb64': uidb64, 'token': token})
            absurl = "http://" + current_site + relative_link
            email = send_mail("Account confirmation mail", f"Click on the link to activate your account {absurl}",
                              settings.EMAIL_HOST_USER, [email])
    else:
        form = RegisterForm()
    context = {
        "register_form": form
    }
    return render(request, 'users/register.html', context)


def activate(request, uidb64, token):
    try:
        id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=id)
        if not default_token_generator.check_token(user, token):
            return "Invalid token"
        user.is_active = True
        user.save()
        messages.success(request, "Your account has been activated!")
        return redirect("login")
    except DjangoUnicodeDecodeError as identifier:
        if not default_token_generator.check_token(user, token):
            return "Invalid token"


@login_required
def upload_file(request):
    if request.method == "POST":
        form = UserUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
<<<<<<< HEAD
            messages.success(
                request, "Your file has been uploaded successfully")
=======
            messages.success(request, "Your file has been uploaded successfully")
>>>>>>> 9df44f8c17e2e11d51061678e55b47475495b5ec
    else:
        form = UserUploadForm()

    context = {
        "upload_form": form
    }
    return render(request, "users/upload_file.html", context)
