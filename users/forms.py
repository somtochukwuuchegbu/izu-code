from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import UserUpload
from django import forms


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(required=True, max_length=200)
    last_name = forms.CharField(required=True, max_length=200)
    # is_active = forms.CheckboxInput()

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "username",
            "email",
            "password1",
            "password2",
        ]


class UserUploadForm(forms.ModelForm):
    user_file = forms.FileField()

    class Meta:
        model = UserUpload
        fields = [
            "user_file",
        ]
