from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator

class UserUpload(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_file = models.FileField(upload_to="csv_files", validators=[FileExtensionValidator(['xlsx'])])

    def __str__(self):
        return f"Uploaded by {self.user.username}"

# Create your models here.
